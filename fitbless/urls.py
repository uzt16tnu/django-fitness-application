from django.contrib import admin
from django.urls import path, include 
from django.contrib.auth import views as auth_views
from users import views as core_views
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', core_views.home, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='login'), name='logout'),
    path('signup/', core_views.signup, name='signup'),
    path('update/', core_views.update, name='update'),
    path('logfood/', core_views.logFood, name='logfood'),
    path('logexercise/', core_views.logExercise, name='logexercise'),
    path('weightgraph/', core_views.weightGraph, name='weightgraph'),
    path('caloriesgraph/', core_views.caloriesGraph, name='caloriesgraph'),
]
