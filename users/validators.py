from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from datetime import date
from dateutil.relativedelta import relativedelta

def validateDoB(value):
    threshold = date.today() - relativedelta(years=12)
    if value > threshold :
        raise ValidationError('You must be at least 12 years old')

def validateWeightDate(value):
    today = date.today()
    threshold = date.today() - relativedelta(days=7)
    if value > today :
        raise ValidationError("Date is in the future")
    elif value < threshold :
        raise ValidationError("Date can't be more than a week old")

def validateFoodDate(value):
    today = date.today()
    threshold = date.today() - relativedelta(days=1)
    if value > today :
        raise ValidationError("Date is in the future")
    elif value < threshold :
        raise ValidationError("Date must be today")

def validateExerciseDate(value):
    today = date.today()
    threshold = date.today() - relativedelta(days=1)
    if value > today :
        raise ValidationError("Date is in the future")
    elif value < threshold :
        raise ValidationError("Date must be today")
