from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User, WeightLog, FoodLog, ExerciseLog
from .validators import validateDoB, validateWeightDate, validateFoodDate, validateExerciseDate

class UserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='First Name')
    last_name = forms.CharField(max_length=30, required=True, help_text='Last Name')
    email = forms.EmailField(max_length=254, required=True, help_text='Email')
    date_of_birth = forms.DateField(required=True, help_text='DoB', validators=[validateDoB], input_formats=['%d/%m/%Y', '%d/%m/y'])
    weight = forms.DecimalField(max_value=500, decimal_places=2, required=True)
    height = forms.IntegerField(max_value=300, required=True)
    activity_level = forms.ChoiceField(choices=User.ACTIVITY_LEVEL_CHOICES, required=True)
    gender = forms.ChoiceField(choices=User.GENDER_CHOICES, required=True)

    class Meta(UserCreationForm):
        model = User
        fields = ('email', 'password1', 'password2', 'first_name', 'last_name',
                  'date_of_birth', 'weight', 'height',
                  'activity_level','gender', )

class UserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('email', 'weight', 'activity_level')

class UpdateUserWeightForm(forms.ModelForm):
    date = forms.DateField(required=True, input_formats=['%d/%m/%Y', '%d/%m/y'], validators=[validateWeightDate])
    weight = forms.DecimalField(max_value=500, decimal_places=2, required=True, help_text='Enter new weight in kg')

    class Meta():
        model = WeightLog
        fields = ('date', 'weight',)

class FoodLogForm(forms.ModelForm):
    date = forms.DateField(required=True, input_formats=['%d/%m/%Y', '%d/%m/y'], validators=[validateFoodDate])
    calories_consumed = forms.IntegerField(help_text='Enter calories in kcal')

    class Meta():
        model = FoodLog
        fields = ('date', 'calories_consumed',)

class ExerciseLogForm(forms.ModelForm):
    date = forms.DateField(required=True, input_formats=['%d/%m/%Y', '%d/%m/y'], validators=[validateExerciseDate])
    calories_burnt = forms.IntegerField(help_text='Enter calories in kcal')

    class Meta():
        model = ExerciseLog
        fields = ('date', 'calories_burnt',)
