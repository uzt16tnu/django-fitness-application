from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _
from decimal import *
from datetime import date
import plotly.graph_objects as go
from plotly.offline import plot
from dateutil.relativedelta import relativedelta

# Create your models here.

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractUser):
    ACTIVITY_LEVEL_CHOICES = [
        ('L', 'Low'),
        ('M', 'Medium'),
        ('H', 'High'),
    ]
    GENDER_CHOICES = [
        ('M','Male'),
        ('F','Female'),
    ]
    #  pass
    username = None
    email = models.EmailField(unique=True, null=True)
    date_of_birth = models.DateField()
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    height = models.IntegerField()
    activity_level = models.CharField(max_length=1, choices=ACTIVITY_LEVEL_CHOICES, default='M')
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def calcBMI(self):
        decimalHeight = Decimal(self.height)/100
        return round(self.weight/(decimalHeight**2), 2)

    def calcBMR(self):
        decimalHeight = Decimal(self.height)
        decimalWeight = Decimal(self.weight)
        age = self.calcAge()
        bmr = (10 * decimalWeight) + (Decimal(6.25) * decimalHeight) - (5 * age)
        bmr = int(bmr + 5 if self.gender == 'M' else bmr - 161)
        bmr = bmr * self.activityMapping()
        return int(bmr) 

    def calcAge(self):
        today = date.today()
        return today.year - self.date_of_birth.year - ((today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))

    def activityMapping(self):
        switcher = {
            'L': 1.375,
            'M': 1.55,
            'H': 1.725,
        }
        return switcher.get(self.activity_level) 

    def weightGraph(self, pastDate = date.today() - relativedelta(months=1)):
        dates = list(WeightLog.objects.filter(user_id=self.id).values_list('date', flat=True))
        weights = list(WeightLog.objects.filter(user_id=self.id).values_list('weight', flat=True))
        chart = go.Figure(data=[go.Scatter(x=dates, y=weights)])
        chart.update_layout(title=go.layout.Title(text='Weight over Time'),
                            xaxis=go.layout.XAxis(title=go.layout.xaxis.Title(text='Date')),
                            yaxis=go.layout.YAxis(title=go.layout.yaxis.Title(text='Weight(kg)')),
                            xaxis_range=[pastDate,WeightLog.objects.latest('id').date])
        return plot(chart, output_type='div', include_plotlyjs=False)

    def caloriesGraph(self, pastDate = date.today() - relativedelta(months=1)):
        datesFood = list(FoodLog.objects.filter(user_id=self.id).values_list('date', flat=True))
        caloriesConsumed = list(FoodLog.objects.filter(user_id=self.id).values_list('calories_consumed', flat=True))
        datesExercise = list(ExerciseLog.objects.filter(user_id=self.id).values_list('date', flat=True))
        caloriesBurned = list(ExerciseLog.objects.filter(user_id=self.id).values_list('calories_burnt', flat=True))

        chart = go.Figure()
        chart.add_trace(go.Scatter(x=datesFood, y=caloriesConsumed, name='Calories Consumed'))
        chart.add_trace(go.Scatter(x=datesExercise, y=caloriesBurned, name='Calories Burned'))
        chart.update_layout(title=go.layout.Title(text='Calories over Time'),
                            xaxis=go.layout.XAxis(title=go.layout.xaxis.Title(text='Date')),
                            yaxis=go.layout.YAxis(title=go.layout.yaxis.Title(text='Calories(kcal)')),
                            xaxis_range=[pastDate,date.today()])
                            #  )
        return plot(chart, output_type='div', include_plotlyjs=False)

class WeightLog(models.Model):
    date = models.DateField(null=True)
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class FoodLog(models.Model):
    date = models.DateField(null=True)
    calories_consumed = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class ExerciseLog(models.Model):
    date = models.DateField(null=True)
    calories_burnt = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
