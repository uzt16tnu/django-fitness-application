from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import get_object_or_404, render, redirect

from users.forms import UserCreationForm, UpdateUserWeightForm, FoodLogForm, ExerciseLogForm
from users.models import User, WeightLog, FoodLog, ExerciseLog
from datetime import date, datetime

@login_required(login_url='/login/')
def home(request):
    qs = FoodLog.objects.filter(pk=request.user.id, date=date.today())
    calories_consumed = qs[0].calories_consumed if qs.count() > 0 else 0
    qs = ExerciseLog.objects.filter(pk=request.user.id, date=date.today())
    calories_burned = qs[0].calories_burnt if qs.count() > 0 else 0
    calories_left = request.user.calcBMR()
    calories_left = calories_left - calories_consumed + calories_burned
    return render(request, 'home.html', {'calories_consumed': calories_consumed,
                                         'calories_burned': calories_burned,
                                         'calories_left': calories_left})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

def update(request):
    if request.method == 'POST':
        form = UpdateUserWeightForm(request.POST)
        if form.is_valid():
            try:
                log = WeightLog.objects.get(date=form.cleaned_data.get('date'), user_id = request.user.id)
                log.weight = form.cleaned_data.get('weight')
            except:
                log = form.save(commit=False)
                log.user = request.user
            log.save()
            User.objects.filter(pk=request.user.id).update(weight = log.weight)
            return redirect('home')
    else:
        form = UpdateUserWeightForm()
    return render(request, 'updateweight.html', {'form': form})

def logFood(request):
    if request.method == 'POST':
        form = FoodLogForm(request.POST)
        if form.is_valid():
            try:
                log = FoodLog.objects.get(date=form.cleaned_data.get('date'), user_id = request.user.id)
                log.calories_consumed = form.cleaned_data.get('calories_consumed') + log.calories_consumed
            except:
                log = form.save(commit=False)
                log.user = request.user
            log.save()
            return redirect('home')
    else:
        form = FoodLogForm()
    return render(request, 'foodlog.html', {'form': form})

def logExercise(request):
    if request.method == 'POST':
        form = ExerciseLogForm(request.POST)
        if form.is_valid():
            try:
                log = ExerciseLog.objects.get(date=form.cleaned_data.get('date'), user_id = request.user.id)
                log.calories_burnt = form.cleaned_data.get('calories_burnt') + log.calories_burnt
            except:
                log = form.save(commit=False)
                log.user = request.user
            log.save()
            return redirect('home')
    else:
        form = ExerciseLogForm()
    return render(request, 'exerciselog.html', {'form': form})

def weightGraph(request):
    try:
        past = request.GET.get('pastdate')
        past = datetime.strptime(past, '%Y-%m-%d').date()
        plot = request.user.weightGraph(past)
    except:
        plot = request.user.weightGraph()
    return  render(request, 'weightchart.html', {'plot': plot})

def caloriesGraph(request):
    try:
        past = request.GET.get('pastdate')
        past = datetime.strptime(past, '%Y-%m-%d').date()
        plot = request.user.caloriesGraph(past)
    except:
        plot = request.user.caloriesGraph()
    return  render(request, 'calorieschart.html', {'plot': plot})
